/*
 * ADS1115 - 16 bit ADC.
 * In order to work correctly for the sound level in dB, continuous
 * mode has to be activated.
 *
 */

#include <stdio.h>
#include <math.h>

#include "driver/i2c.h"
#include "ADS1115.h"

i2c_cmd_handle_t _cmd;

/*
 * @Brief. Initialization of ADS1115
 * 4.096v
 * Continuous mode
 * Channel 0
 */
void ADS1115_initialization()
{
	uint16_t adsConfig = 	ADS1115_COMP_QUE_OFF |
			ADS1115_COMP_LAT_OFF |
			ADS1115_COMP_POL_LWO |
			ADS1115_COMP_MODE_DEFAULT |
			ADS1115_DR_128 |
			ADS1115_MODE_CONTINIOUS |
			ADS1115_PGA_4V |
			ADS1115_MUX_AIN0 |
			ADS1115_OS_NO_EFFECT;

	uint8_t highByteConfig = adsConfig >> 8;
	uint8_t lowByteConfig = adsConfig & 0xFF;

	_cmd = i2c_cmd_link_create();
	i2c_master_start(_cmd); //Request I2C start
	i2c_master_write_byte(_cmd, (ADS1115_ADDR << 1) | I2C_MASTER_WRITE, 1); //@args cmd_handle - I2C cmd link, data - data to send, ack_en - enable ack check for master
	i2c_master_write_byte(_cmd, ADS1115_REG_POINTER_CONFIG, 1);
	i2c_master_write_byte(_cmd, highByteConfig, 0);
	i2c_master_write_byte(_cmd, lowByteConfig, 1);
	i2c_master_stop(_cmd); //I2C command population is complete
	i2c_master_cmd_begin(I2C_NUM_0, _cmd, 1000 / portTICK_RATE_MS); //Ask ESP32 to perform I2C command
	i2c_cmd_link_delete(_cmd); //Release command handle
}

/*
 * @Brief Use to get conversion in a single-shot mode.
 * return uint16_t reading
 */
uint16_t ADS1115_getConversion()
{
	uint8_t _MSB = 0;
	uint8_t _LSB = 0;
	uint16_t result = 0;

	ADS1115_initialization();

	_cmd = i2c_cmd_link_create();
	i2c_master_start(_cmd); //Request I2C start
	i2c_master_write_byte(_cmd, (ADS1115_ADDR << 1) | I2C_MASTER_WRITE, 1);
	i2c_master_write_byte(_cmd, ADS1115_REG_POINTER_CONVERT, 1);
	i2c_master_stop(_cmd);
	i2c_master_cmd_begin(I2C_NUM_0, _cmd, 500 / portTICK_RATE_MS); //Ask ESP32 to perform I2C command
	i2c_cmd_link_delete(_cmd); //Release command handle

	_cmd = i2c_cmd_link_create();
	i2c_master_start(_cmd); //Request I2C start
	i2c_master_write_byte(_cmd, (ADS1115_ADDR << 1) | I2C_MASTER_READ, 1);
	i2c_master_read_byte(_cmd, &_MSB, 1);
	i2c_master_read_byte(_cmd, &_LSB, 1);
	i2c_master_stop(_cmd);
	i2c_master_cmd_begin(I2C_NUM_0, _cmd, 500 / portTICK_RATE_MS); //Ask ESP32 to perform I2C command
	i2c_cmd_link_delete(_cmd); //Release command handle

	result = _MSB << 8 | _LSB;

	return result;
}

/*
 * @Brief Read measurments in continuous mode.
 * Convert measurement into dB.
 * @ samples amount of samples. To high sample amount
 * may cause variable overflow (850 is good value).
 * return double sound level in dB
 */
double ADS1115_getConversion_Continuous(uint32_t samples)
{
	float peakToPeak = 0;   //Peak to peak level
	uint32_t currentReading = 0; 	//Value of current reaing
	long signalMax = 0;		//Signal maximum
	long signalMin = 32767; //Signal minimum
	uint32_t measurmentCounter = 0; //Measurement counter

	unsigned long peakToPeakAverage = 0;
	double p2pRMS = 0;


	for(measurmentCounter = 0; measurmentCounter < samples; measurmentCounter++)
	{
		//Get conversion from ADS1115
		currentReading = ADS1115_getConversion();

		signalMax = maxValue(currentReading, signalMax); //Get minimum reading
		signalMin = minValue(currentReading, signalMin); //Get maximum reading

		peakToPeak = signalMax - signalMin;  //Calculate peak to peak (amplitude)
		peakToPeakAverage += peakToPeak*peakToPeak; //Average peak to peak values
	}

	p2pRMS = peakToPeakAverage/samples; //Average peak to peak value

	//If ADS1115 will read 0 peak-to-peak (because of low signal and/or sample rate) set to default 256*256
	if(peakToPeakAverage == 0)
	{
		peakToPeakAverage = 256*256;
	}
	p2pRMS = (sqrt(peakToPeakAverage))*0.707; //Peak to peak RMS

	double dB = 20.0*log(p2pRMS/(double)256); //256

	return (dB); //Offset
}

/*
 * @Brief debug function that prints voltage reading in 4.096 V single-shot mode
 */
void ADS115_printConversion()
{
#ifdef ADS1115_DEBUGON
	printf("ADS1115 conversion: %f.\n", (ADS1115_getConversion()*0.1250038)/1000);
#endif
}

/*
 * @brief Find lowest value
 * @valueONe long first value
 * @valueTwo long second value
 * return lowest value
 */
long minValue(long valueOne, long valueTwo)
{
	return valueOne < valueTwo ? valueOne : valueTwo;
}

/*
 * @brief Find highest value
 * @valueONe long first value
 * @valueTwo long second value
 * return highest value
 */
long maxValue(long valueOne, long valueTwo)
{
	return valueOne > valueTwo ? valueOne : valueTwo;
}
