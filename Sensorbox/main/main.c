/*============================================================================
Name:          main.c
Author:        Roman Grintsevich
Compile with:  Eclipse IDE for C/C++ Developers. Version: Neon.3 Release (4.6.3)
Date:          2017-07-24
Description:   Project sensorbox including 4 sensors.
			   1. TSL2561 - Measures ambient light in lux.
			   2. ADS1115 - 16 ADC with OP amp and microphone, measuring sound in dB.
			   3. Si7021 - Measures ambient temperature in Celsius and relative humidity in %.
			   4. CCS811 - Measures CO2 in ppm and tVOC in ppb.
============================================================================*/

#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_spi_flash.h"
#include "sdkconfig.h"
#include "driver/i2c.h" //I2C
#include "driver/gpio.h" //GPIO used for LEDs
#include "esp_event.h" //WIFI, TCP/IP
#include "esp_event_loop.h"
#include "esp_wifi.h" //WIFI
#include "tcpip_adapter.h"
//#include <esp_err.h>
#include "nvs_flash.h"
#include "lwip/sockets.h" //Used for TCP/IP
#include <math.h> //Calculations
#include "errno.h"

//Sensor includes
#include "sensorbox.h"
#include "TSL2561.h"
#include "Si7021.h"
#include "CCS811.h"
#include "ADS1115.h"

//DEBUG (scans I2C bus, prints address of connected devices on I2C)
#include "I2CScanner.h"

//Season (0 summer and winter 1)
uint8_t season = 0;

//Bounds for all measurements
uint32_t temperatureLowerBound = 22;
uint32_t temperatureUpperBound = 24;
uint32_t humidityLowerBound = 30; //40
uint32_t humidityUpperBound = 60;
uint32_t lightLowerBound = 200;
uint32_t lightUpperBound = 500;
uint32_t soundUpperBound = 85;
uint32_t co2UpperBound = 800;

//Measurements
uint16_t gCO2 = 0;
uint16_t gtVOC = 0;
double gsound = 0;
float gtemperature = 0;
float ghumidity = 0;
uint32_t glight = 0;

//Used to point on what LED to lit.
typedef void(*ledStatus)(void);

//Pointers to the led functions
ledStatus ledToTurnOn[] =
{
		redLedOn,
		orangeLedOn,
		greenLedOn
};

//Used to point on what current season is.
typedef void(*seasons)(void);

//Pointers to the led functions
seasons currentSeason[] =
{
		summerSeason,
		winterSeason
};

/*
 * @Brief.  Set up ESP32 I2C with clock speed 100kHz and pull-up on SCL and SDA lines.
 */
void ESP_I2C_Init()
{
	printf("Initializing ESP32 I2C.\n");

	//Configure ESP32 I2C as master
	i2c_config_t conf;
	conf.mode = I2C_MODE_MASTER; //Set ESP32 as master
	conf.sda_io_num = SDA_PIN;
	conf.scl_io_num = SCL_PIN;
	conf.sda_pullup_en = GPIO_PULLUP_ENABLE;
	conf.scl_pullup_en = GPIO_PULLUP_ENABLE;
	conf.master.clk_speed = 100000; //Clock speed of I2C
	i2c_param_config(I2C_NUM_0, &conf);
	i2c_driver_install(I2C_NUM_0, I2C_MODE_MASTER, 0, 0, 0); //Enable I2C
}

/*
 * @Brief. I2C task reads sensors measurements.
 */
void i2c_sensors(void* arg)
{
	//ESP32 I2C initialization
	ESP_I2C_Init();

	//Sensors initialization
	TSL2561_initialization();
	CCS811_initialization();
	CCS811_startApp();


	for(;;)
	{
		//Get light level in lux
#ifdef TSL2561_LightSensor
		glight = TSL2561_getLux();
		printf("%d lux.\n", glight);
#endif

		//Get ADC value (microphone)
#ifdef ADS1115_SoundSensor
		gsound = ADS1115_getConversion_Continuous(850);

		if(gsound < 60)
		{
			printf("dB: <60 dB.\n");
		}
		else
		{
			printf("dB: %.2f dB.\n", gsound);
		}

#endif

		//Get temperature and humidity
#ifdef Si7021_TemperatureHumiditySensor
		ghumidity = Si7021_getHumidity();
		gtemperature = Si7021_getTemperature();
		printf("%.2f\%%. \n%.2f C.\n", ghumidity, gtemperature);
#endif

		//Get CO2 and tVOC
#ifdef CCS811_AirSensor
		if(dataReady())
		{
			CCS811_read_ALG_RESULT_DATA();
			gCO2 = CCS811_getCO2();
			gtVOC = CCS811_gettVOC();
		}
		else if(checkError())
		{
			printf("CCS811 ERROR.\n");
		}
		printf("%d ppm. \n%d ppb.\n\n", gCO2, gtVOC);
#endif
		//Measurements delay
		vTaskDelay(measurmentDelay/portTICK_PERIOD_MS);
	}
}

/*
 * @Green led: all measurements are good.
 * @Orange led: one or more values are out of bounds.
 * @Red led: Sound or CO2 values are out of bounds.
 *
 * Temperature 20-22 winter, 22-24 summer
 * Humidity 40% - 60%
 * CO2 max 800 ppm
 * Light 200 - 500
 * Sound up to 85 dB
 */
void led_notification(void* arg)
{
	//Initialize led pins.
	gpio_pad_select_gpio(GPIO_NUM_13);
	gpio_set_direction(GPIO_NUM_13, GPIO_MODE_OUTPUT);
	gpio_pad_select_gpio(GPIO_NUM_14);
	gpio_set_direction(GPIO_NUM_14, GPIO_MODE_OUTPUT);
	gpio_pad_select_gpio(GPIO_NUM_15);
	gpio_set_direction(GPIO_NUM_15, GPIO_MODE_OUTPUT);

	for(;;)
	{
		ledToTurnOn[checkMeasurment()]();
		vTaskDelay(5000/portTICK_PERIOD_MS);
	}
}

/*
 * @Brief. Wifi task. ESP32 configured as station mode.
 */
#ifdef ESP32_WIFI
void wifi_task(void* arg)
{
	printf("Connecting to access point...\n");
	tcpip_adapter_init();
	esp_event_loop_init(event_handler, NULL);
	wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
	esp_wifi_init(&cfg);
	esp_wifi_set_storage(WIFI_STORAGE_RAM);
	esp_wifi_set_mode(WIFI_MODE_STA);
	wifi_config_t statationMode_config =
	{
			.sta =
			{
					.ssid = SSID_NAME,
					.password = SSID_PASSWORD,
					.bssid_set = false
			}
	};
	esp_wifi_set_config(WIFI_IF_STA, &statationMode_config);
	esp_wifi_start();
	esp_wifi_connect();
}
#endif

/*
 * @Brief. Wifi event handler.
 */
esp_err_t event_handler(void *ctx, system_event_t *event)
{
	if (event->event_id == SYSTEM_EVENT_STA_GOT_IP)
	{
		printf("Connected to WIFI as station mode. Got IP.\n");
	}
	return ESP_OK;
}

/*
 * @Brief. TCP/IP server setup.
 */
#ifdef ESP32_WIFI
void socketServer()
{
	struct sockaddr_in clientAddress;
	struct sockaddr_in serverAddress;

	uint8_t TCPserverRunning = 1;

	//Create a socket
	int sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (sock < 0)
	{
		printf("socket: %d", sock);
		TCPserverRunning = 0;
	}

	//Bind server socket to a port
	serverAddress.sin_family = AF_INET;
	serverAddress.sin_addr.s_addr = htonl(INADDR_ANY);
	serverAddress.sin_port = htons(PORT_NUMBER);
	int rc  = bind(sock, (struct sockaddr *)&serverAddress, sizeof(serverAddress));
	if (rc < 0)
	{
		printf("bind: %d", rc);
		TCPserverRunning = 0;
	}

	//Set socket to listen for incoming connections (size of queue 1)
	rc = listen(sock, 1);
	if (rc < 0)
	{
		printf("listen: %d", rc);
		TCPserverRunning = 0;
	}

	while(TCPserverRunning)
	{
		//Listen for a new client connection
		socklen_t clientAddressLength = sizeof(clientAddress);

		//Accept client connection
		int clientSock = accept(sock, (struct sockaddr *)&clientAddress, &clientAddressLength);

		printf("#Debug. Client connected.\n");

		//Send notification to client upon success connection
		char *welcomeMessage = "Connected to sensorbox.\n";
		send(clientSock, welcomeMessage, 24, 0);

		if (clientSock < 0)
		{
			printf("accept: %d", clientSock);
			TCPserverRunning = 0;
		}

		//Save data from the client
		int total =	10*1024;
		int sizeUsed = 0;
		char *data = malloc(total);

		//Loop reading data
		while(1)
		{
			//Receive data from clint
			ssize_t sizeRead = recv(clientSock, data + sizeUsed, total-sizeUsed, 0);

			if(sizeRead > 0)
			{
				char *answer = "Sending data.\n";
				printf("#Debug. Data from client (size: %d) value: %s", sizeUsed, data);
				send(clientSock, answer, 14, 0); //Send confirmation to client on successfully received data
			}

			if (sizeRead == 0)
			{
				break;
			}
			sizeUsed += sizeRead;
		}

		// Finished reading data.
		printf("Data read (size: %d) was: %s", sizeUsed, data);
		free(data);
		close(clientSock);
	}

	vTaskDelete(NULL);
}
#endif

/*
 * @Brief. Set red led on. Turn off orange and green.
 */
void redLedOn(void)
{
	gpio_set_level(GPIO_NUM_13, 0);
	gpio_set_level(GPIO_NUM_14, 0);
	gpio_set_level(GPIO_NUM_15, 1);
}

/*
 * @Brief. Set orange led on. Turn off red and green.
 */
void orangeLedOn(void)
{
	gpio_set_level(GPIO_NUM_13, 0);
	gpio_set_level(GPIO_NUM_14, 1);
	gpio_set_level(GPIO_NUM_15, 0);
}

/*
 * @Brief. Set green led on. Turn off red and orange.
 */
void greenLedOn(void)
{
	gpio_set_level(GPIO_NUM_13, 1);
	gpio_set_level(GPIO_NUM_14, 0);
	gpio_set_level(GPIO_NUM_15, 0);
}

/*
 * @Brief. Check if any measurement is out of bounds.
 */
uint8_t checkMeasurment(void)
{
	if(gsound > soundUpperBound || gCO2 > co2UpperBound)
	{
		return 0;
	}
	else if(gtemperature < temperatureLowerBound || gtemperature > temperatureUpperBound || ghumidity < humidityLowerBound || ghumidity > humidityUpperBound || glight < lightLowerBound || glight > lightUpperBound)
	{
		return 1;
	}
	else
	{
		return 2;
	}
}

/*
 * @Brief. Sets temperatures bounds to summer time (22-24 °С).
 */
void summerSeason(void)
{
	temperatureLowerBound = 22;
	temperatureUpperBound = 24;
}

/*
 * @Brief. Sets temperatures bounds to winter time (20-22 °С).
 */
void winterSeason(void)
{
	temperatureLowerBound = 20;
	temperatureUpperBound = 22;
}

/*
 * @Brief. Set season. 0 - summer, 1 - winter
 * Call setSeason(0 or 1);
 * Next call is currentSeason[season]();
 */
void setSeason(uint8_t inputSeason)
{
	season = inputSeason; //Set global season variable
	currentSeason[season](); //Change temperature bounds (this row can be used at any point of the runtime)
}

void app_main()
{
	//nvs_flash_init();

	//Scans I2C bus for connected device on I2C
	//scanI2C();

	//Set season to summer (default)
	setSeason(0);

	vTaskDelay(1000/portTICK_PERIOD_MS);

#ifdef ESP32_WIFI
	//WIFI task
	xTaskCreate(wifi_task, "wifi_task_0", configMINIMAL_STACK_SIZE, NULL, 9, NULL);
	//TCP server task
	//xTaskCreate(socketServer, "socketServer_0", configMINIMAL_STACK_SIZE, NULL, 9, NULL);
#endif

	//Starts I2C task and LED task.
	xTaskCreate(i2c_sensors, "i2c_sensors_0", configMINIMAL_STACK_SIZE, NULL, 9, NULL);
	xTaskCreate(led_notification, "led_notification_0", configMINIMAL_STACK_SIZE, NULL, 9, NULL);
}


