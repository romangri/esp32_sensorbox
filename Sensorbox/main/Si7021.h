#ifndef _tempHumid7021_H_
#define _tempHumid7021_H_

//Enables debug output
//#define Si7021_DEBUGON

#define Si7021_ADDR 0x40 //Default I2C address

#define HUMD_MEASURE_NOHOLD 0xF5
#define TEMP_PREV   		0xE0

void Si7021_initialization();
void Si7021_getSensorID();
uint8_t Si7021_printSensorId();
uint16_t Si7021_startMeasurment(uint8_t cmd);
void Si7021_printRH();
void Si7021_printTemp();
float Si7021_getTemperature();
float Si7021_getHumidity();

#endif
