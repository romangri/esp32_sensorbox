#ifndef _air881_H_
#define _air881_H_

//Enables debug output
//#define CCS811_DEBUGON

#define CCS811_ADDR 0x5B //Default I2C address

#define STATUS_REG 			0x00
#define CCS811_MEAS_MODE 	0x01
#define CCS811_ALG_RESULT_DATA 	0x02
#define CCS811_HW_ID 		0x20
#define CCS811_HW_VERSION	0x21
#define CCS811_FW_BOOT_V	0x23
#define CCS811_FW_APP_V		0x24
#define CCS811_ERROR_REG	0xE0
#define CCS811_APP_START 	0xF4
#define CCS811_SW_RESET		0xFF


//Pick measurment mode
//#define CCS811_MODE	0 //Idle
//#define CCS811_MODE		1 //Constant power mode, measurement every 1 second
#define CCS811_MODE	2 //Pulse heating mode, measurement every 10 seconds
//#define CCS811_MODE	3 //Low power pulse heating mode, measurement every 60 seconds
//#define CCS811_MODE	4 //Constant power mode, measurement every 250 ms

void CCS811_initialization();
void readOneBye(uint8_t *id, uint8_t reg);
void CCS811_getSensorID(uint8_t *id, uint8_t reg);
uint16_t CCS811_getFWinfo(uint8_t reg);
void CCS811_printSensorId();
uint8_t dataReady();
uint8_t readErrorIDRegister();
void CCS811_startApp();
uint8_t CCS811_getMEAS_MODE();
void CCS811_setMEAS_MODE();
void CCS811_read_ALG_RESULT_DATA();
uint16_t CCS811_gettVOC();
uint16_t CCS811_getCO2();
uint8_t checkError();

#endif
