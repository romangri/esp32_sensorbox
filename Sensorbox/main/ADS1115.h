#ifndef _sound1115_H_
#define _sound1115_H_

//Enables debug output
//#define ADS1115_DEBUGON

#define ADS1115_ADDR 0x48 //Default address

#define ADS1115_REG_POINTER_CONFIG 	0x01
#define ADS1115_REG_POINTER_CONVERT 0x00

#define ADS1115_COMP_QUE_OFF 		0x0003  //Disable comparator (default)
#define ADS1115_COMP_LAT_OFF 		0x0000 	//Non-latching comparator (default)
#define ADS1115_COMP_POL_LWO 		0x0000 	//Active low (default)
#define ADS1115_COMP_MODE_DEFAULT	0x0000	//Traditional comparator with hysteresis (default)
#define ADS1115_DR_128				0x0080 	//128 SPS (default)
#define ADS1115_MODE_SINGLE_SHOT 	0x0100 	//Power-down singke-shot (default)
#define ADS1115_MODE_CONTINIOUS 	0x0000	//Continuous mode
#define ADS1115_PGA_4V				0x0200	//Full-scale range 4.096V
#define ADS1015_PGA_2V   			0x0400 	//Full-scale range 2.048
#define ADS1115_MUX_AIN0			0x4000 	//AINp = AIN0 and AINn = GND
#define ADS1115_OS_SINGLE_CONVERSION 0x8000 //Begin single conversion (when in power-down mode)
#define ADS1115_OS_NO_EFFECT 0x0000 //Begin single conversion (when in power-down mode)

void ADS1115_initialization();
uint16_t ADS1115_getConversion();
void ADS115_printConversion();
double ADS1115_getConversion_Continuous(uint32_t samples);

//Calculation of lowest and highest values
long maxValue(long valueOne, long valueTwo);
long minValue(long valueOne, long valueTwo);

#endif
