/************************************************
 *
 * Channel 0 Visible and IR
 * Channel 1 IR only
 *
 ************************************************/

#include <stdio.h>

#include "driver/i2c.h"
#include "TSL2561.h"

i2c_cmd_handle_t _cmd;

uint8_t dataCh0_low = 0;
uint8_t dataCh0_high = 0;
uint8_t dataCh1_low = 0;
uint8_t dataCh1_high = 0;
uint8_t _partNo = 0;
uint32_t luxMeasurment = 0;

/*
 * @Brief. Sensor initialization.
 */
void TSL2561_initialization()
{
	printf("Initializing TSL2561...\n");
	_cmd = i2c_cmd_link_create();
	i2c_master_start(_cmd); //Request I2C start
	i2c_master_write_byte(_cmd, (TSL2561_ADDR << 1) | I2C_MASTER_WRITE, 1); //@args cmd_handle - I2C cmd link, data - data to send, ack_en - enable ack check for master
	i2c_master_write_byte(_cmd, TSL2561_COMMAND_BIT, 1);
	i2c_master_write_byte(_cmd, TSL2561_CONTROL_POWERON, 1);
	i2c_master_stop(_cmd); //I2C command population is complete
	i2c_master_cmd_begin(I2C_NUM_0, _cmd, 1000 / portTICK_RATE_MS); //Ask ESP32 to perform I2C command
	i2c_cmd_link_delete(_cmd); //Release command handle
}

/*
 * @Brief Read one byte from register and put data at data pointer
 *
 * @param data pointer, register
 */
void readByte(uint8_t reg, uint8_t *data)
{
	_cmd = i2c_cmd_link_create();
	i2c_master_start(_cmd); //Request I2C start
	i2c_master_write_byte(_cmd, (TSL2561_ADDR << 1) | I2C_MASTER_WRITE, 1);
	i2c_master_write_byte(_cmd, reg, 1);
	i2c_master_stop(_cmd);
	i2c_master_cmd_begin(I2C_NUM_0, _cmd, 500 / portTICK_RATE_MS); //Ask ESP32 to perform I2C command
	i2c_cmd_link_delete(_cmd); //Release command handle

	_cmd = i2c_cmd_link_create();
	i2c_master_start(_cmd); //Request I2C start
	i2c_master_write_byte(_cmd, (TSL2561_ADDR << 1) | I2C_MASTER_READ, 1);
	i2c_master_read_byte(_cmd, data, 1);
	i2c_master_stop(_cmd);
	i2c_master_cmd_begin(I2C_NUM_0, _cmd, 500 / portTICK_RATE_MS); //Ask ESP32 to perform I2C command
	i2c_cmd_link_delete(_cmd); //Release command handle
}

/*
 * @Brief Read sensor id register. If TSL2561_DEBUGON is defined, ID will be printed.
 */
void TSL2561_getSensorID()
{
	readByte(TSL2561_COMMAND_BIT | TSL2561_REGISTER_ID, &_partNo); //Read chip ID register
#ifdef TSL2561_DEBUGON
	printf("PARTNO\n");
	printf("PART CODE: %d\n", _partNo);
	if(_partNo == 0x50)
	{
		printf("PART: TSL2561T/FN/CL\n");
	}
	printf("END OF PART NO.\n");
#endif
}

/*
 * @Brief. Read 4 data registers and call calculate lux function, that returns value in lux.
 */
void TSL2561_readSensorData()
{
	readByte(TSL2561_COMMAND_BIT | TSL2561_REGISTER_CH0_LOW, &dataCh0_low); //Address the Ch0 lower data register
	readByte(TSL2561_COMMAND_BIT | TSL2561_REGISTER_CH0_HIGH, &dataCh0_high); //Address the Ch0 upper data register
	readByte(TSL2561_COMMAND_BIT | TSL2561_REGISTER_CH1_LOW, &dataCh1_low); //Address the Ch1 lower data register
	readByte(TSL2561_COMMAND_BIT | TSL2561_REGISTER_CH1_HIGH, &dataCh1_high); //Address the Ch1 upper data register

	uint16_t ch0Data = 0;
	uint16_t ch1Data = 0;
	ch0Data = 256 * dataCh0_high + dataCh0_low;
	ch1Data = 256 * dataCh1_high + dataCh1_low;

	luxMeasurment = calculateLux(ch0Data, ch1Data);

#ifdef TSL2561_DEBUGON
	printf("LUX: %d\n", calculateLux(ch0Data, ch1Data));

	printf("BYTE\n");
	printf("CH0_l: %d\n", dataCh0_low);
	printf("CH0_h: %d\n", dataCh0_high);
	printf("CH1_l: %d\n", dataCh1_low);
	printf("CH1_h: %d\n", dataCh1_high);
	printf("END OF READING.\n");

	printf("WORD\n");
	printf("CH0: %d\n", ch0Data);
	printf("CH1: %d\n", ch1Data);
	printf("END OF READING.\n");
#endif
}

/*
 * @Brief. Calculate lux (see datasheet for more information about calculation).
 * @param ch0 uint16_t measurement from channel 0
 * @param ch1 uint16_t measurement from channel 1
 */
uint32_t calculateLux(uint16_t ch0, uint16_t ch1)
{
	unsigned long chScale;
	unsigned long channel1;
	unsigned long channel0;

	chScale = (1 << TSL2561_LUX_CHSCALE);
	chScale = chScale << 4;

	// Scale the channel values
	channel0 = (ch0 * chScale) >> TSL2561_LUX_CHSCALE;
	channel1 = (ch1 * chScale) >> TSL2561_LUX_CHSCALE;

	//Find the ratio of the channel values (Channel1/Channel0)
	unsigned long ratio1 = 0;
	if (channel0 != 0)
	{
		ratio1 = (channel1 << (TSL2561_LUX_RATIOSCALE+1)) / channel0;
	}

	//Round the ratio value
	unsigned long ratio = (ratio1 + 1) >> 1;

	unsigned int b = 0, m = 0;

	//Datasheet calculation
	if (ratio <= TSL2561_LUX_K1T) //if ((ratio >= 0) && (ratio <= TSL2561_LUX_K1T))
	{
		b=TSL2561_LUX_B1T; m=TSL2561_LUX_M1T;
	}
	else if (ratio <= TSL2561_LUX_K2T)
	{
		b=TSL2561_LUX_B2T; m=TSL2561_LUX_M2T;
	}
	else if (ratio <= TSL2561_LUX_K3T)
	{
		b=TSL2561_LUX_B3T; m=TSL2561_LUX_M3T;
	}
	else if (ratio <= TSL2561_LUX_K4T)
	{
		b=TSL2561_LUX_B4T; m=TSL2561_LUX_M4T;
	}
	else if (ratio <= TSL2561_LUX_K5T)
	{
		b=TSL2561_LUX_B5T; m=TSL2561_LUX_M5T;
	}
	else if (ratio <= TSL2561_LUX_K6T)
	{
		b=TSL2561_LUX_B6T; m=TSL2561_LUX_M6T;
	}
	else if (ratio <= TSL2561_LUX_K7T)
	{
		b=TSL2561_LUX_B7T; m=TSL2561_LUX_M7T;
	}
	else if (ratio > TSL2561_LUX_K8T)
	{
		b=TSL2561_LUX_B8T; m=TSL2561_LUX_M8T;
	}

	unsigned long temp;
	temp = ((channel0 * b) - (channel1 * m));

	temp += (1 << (TSL2561_LUX_LUXSCALE-1));

	//Strip off fractional portion
	uint32_t lux = temp >> TSL2561_LUX_LUXSCALE;

	return lux;
}

/*
 * @Brief. Get measurement in lux.
 * return uint32_t lux
 */
uint32_t TSL2561_getLux()
{
	TSL2561_readSensorData();

	return luxMeasurment;
}
