#ifndef _mainSensorbox_H_
#define _mainSensorbox_H_

//Enable WIFI
//#define ESP32_WIFI

//WIFI ssid and password
#define SSID_NAME "SSID"
#define SSID_PASSWORD "PASSWORD"

//Enable light sensor
#define TSL2561_LightSensor

//Enable temperature and humidity sensor
#define Si7021_TemperatureHumiditySensor

//Enable light sensor
#define CCS811_AirSensor

//Enable light sensor
#define ADS1115_SoundSensor

//TCP/IP server port number
#define PORT_NUMBER 8000

//I2C pins
#define SCL_PIN 19
#define SDA_PIN 18

//GPIO used for LED indication
#define redLED 	GPIO_NUM_15
#define orangeLED GPIO_NUM_14
#define greenLED GPIO_NUM_13

#define measurmentDelay (measurmentDelaySeconds*1000) //Measurement delay in ms
#define measurmentDelaySeconds 20 //Measurement delay in seconds

void redLedOn(void);
void orangeLedOn(void);
void greenLedOn(void);
esp_err_t event_handler(void *ctx, system_event_t *event);
uint8_t checkMeasurment(void);
void setSeason(uint8_t lSeason);
void summerSeason(void);
void winterSeason(void);

#endif
