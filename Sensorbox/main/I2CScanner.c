#include "sdkconfig.h"
#include "driver/i2c.h"
#include "I2CScanner.h"


void scanI2C()
{
	int i = 0;

	printf("     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f\n");
	printf("00:         ");

	esp_err_t espRc;
	for (i=3; i< 0x78; i++)
	{
		i2c_cmd_handle_t cmd = i2c_cmd_link_create();
		i2c_master_start(cmd);
		i2c_master_write_byte(cmd, (i << 1) | I2C_MASTER_WRITE, 1 /* expect ack */);
		i2c_master_stop(cmd);

		espRc = i2c_master_cmd_begin(I2C_NUM_0, cmd, 10/portTICK_PERIOD_MS);
		if(i%16 == 0)
		{
			printf("\n%.2x:", i);
		}
		if(espRc == 0)
		{
			printf(" %.2x", i);
		}
		else
		{
			printf(" --");
		}
		i2c_cmd_link_delete(cmd);
	}

	printf("\n");
}
