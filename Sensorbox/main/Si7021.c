/*
 * Starting first humidity measurement, which will also start
 * temperature measurement.
 *
 */


#include <stdio.h>

#include "driver/i2c.h"
#include "Si7021.h"
#include "sdkconfig.h"

i2c_cmd_handle_t _cmd;

/*
 * @Brief. Sensor initialization.
 */
void Si7021_initialization()
{
	_cmd = i2c_cmd_link_create();
	i2c_master_start(_cmd); //Request I2C start
	i2c_master_write_byte(_cmd, (Si7021_ADDR << 1) | I2C_MASTER_WRITE, 1); //@args cmd_handle - I2C cmd link, data - data to send, ack_en - enable ack check for master
	i2c_master_stop(_cmd); //I2C command population is complete
	i2c_master_cmd_begin(I2C_NUM_0, _cmd, 1000 / portTICK_RATE_MS); //Ask ESP32 to perform I2C command
	i2c_cmd_link_delete(_cmd); //Release command handle
}

/*
 * @Brief. Read sensor id register.
 * @param id pointer whhere to store sensor ID.
 */
void Si7021_getSensorID(uint8_t *id)
{
	_cmd = i2c_cmd_link_create();
	i2c_master_start(_cmd); //Request I2C start
	i2c_master_write_byte(_cmd, (Si7021_ADDR << 1) | I2C_MASTER_WRITE, 1);
	i2c_master_write_byte(_cmd, 0xFC, 1);
	i2c_master_write_byte(_cmd, 0xC9, 1);
	i2c_master_stop(_cmd);
	i2c_master_cmd_begin(I2C_NUM_0, _cmd, 500 / portTICK_RATE_MS); //Ask ESP32 to perform I2C command
	i2c_cmd_link_delete(_cmd); //Release command handle

	_cmd = i2c_cmd_link_create();
	i2c_master_start(_cmd); //Request I2C start
	i2c_master_write_byte(_cmd, (Si7021_ADDR << 1) | I2C_MASTER_READ, 1);
	i2c_master_read_byte(_cmd, id, 1);
	i2c_master_stop(_cmd);
	i2c_master_cmd_begin(I2C_NUM_0, _cmd, 500 / portTICK_RATE_MS); //Ask ESP32 to perform I2C command
	i2c_cmd_link_delete(_cmd); //Release command handle
}

/*
 * @Brief. Print sensor ID.
 * return uint8_t sensor ID value.
 */
uint8_t Si7021_printSensorId()
{
	uint8_t Si7021_ID = 0;
	Si7021_getSensorID(&Si7021_ID);

#ifdef Si7021_DEBUGON
	if(Si7021_ID == 0x15)
	{
		printf("Si7021 with id: %d.\n", Si7021_ID);
	}
#endif
	return Si7021_ID;
}

/*
 * @Brief. Start humidity measurement. 30 ms delay after measurement start.
 * @ return data from the sensor
 */
uint16_t Si7021_startMeasurment(uint8_t cmd)
{
	uint8_t _MSB = 0;
	uint8_t _LSB = 0;
	uint16_t result = 0;

	_cmd = i2c_cmd_link_create();
	i2c_master_start(_cmd); //Request I2C start
	i2c_master_write_byte(_cmd, (Si7021_ADDR << 1) | I2C_MASTER_WRITE, 1);
	i2c_master_write_byte(_cmd, cmd, 1);
	i2c_master_stop(_cmd);
	i2c_master_cmd_begin(I2C_NUM_0, _cmd, 500 / portTICK_RATE_MS); //Ask ESP32 to perform I2C command
	i2c_cmd_link_delete(_cmd); //Release command handle

	vTaskDelay(30/portTICK_PERIOD_MS); //Conversion time 22 ms

	_cmd = i2c_cmd_link_create();
	i2c_master_start(_cmd); //Request I2C start
	i2c_master_write_byte(_cmd, (Si7021_ADDR << 1) | I2C_MASTER_READ, 1);
	i2c_master_read_byte(_cmd, &_MSB, 1);
	i2c_master_read_byte(_cmd, &_LSB, 1);
	i2c_master_stop(_cmd);
	i2c_master_cmd_begin(I2C_NUM_0, _cmd, 500 / portTICK_RATE_MS); //Ask ESP32 to perform I2C command
	i2c_cmd_link_delete(_cmd); //Release command handle

	_LSB &= 0xFC;

	result = _MSB << 8 | _LSB;

	return result;
}

/*
 * @Brief. Dbug function that prints relative humidity.
 */
void Si7021_printRH()
{
#ifdef Si7021_DEBUGON
	uint16_t rh = Si7021_startMeasurment(HUMD_MEASURE_NOHOLD);
	printf("RH MSB + LSB: %d.\n", rh);
	float RH_result = (125.0*rh/65536)-6;
	printf("RH: %.3f.\n", RH_result);
#endif

}

/*
 * @Brief. Debug function that prints temperature.
 */
void Si7021_printTemp()
{
#ifdef Si7021_DEBUGON
	uint16_t temp = Si7021_startMeasurment(TEMP_PREV);
	printf("TEMP MSB + LSB: %d.\n", temp);
	float temp_result = (175.25*temp/65536)-46.85;
	printf("TEMP: %.3f.\n", temp_result);
#endif
}

/*
 * @Brief. Get measurement and convert it to celsius.
 * @return float temperature
 */
float Si7021_getTemperature()
{
	float temperature = Si7021_startMeasurment(TEMP_PREV);
	temperature = (175.25*temperature/65536)-46.85;
	return temperature;
}

/*
 * @Brief. Get measurement and convert it to %
 * @return float humidity
 */
float Si7021_getHumidity()
{
	float relativeHumidity = Si7021_startMeasurment(HUMD_MEASURE_NOHOLD);
	relativeHumidity = (125.0*relativeHumidity/65536)-6;
	return relativeHumidity;
}
