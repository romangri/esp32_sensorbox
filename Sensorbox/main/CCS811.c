#include <stdio.h>

#include "driver/i2c.h"
#include "CCS811.h"

i2c_cmd_handle_t _cmd;
uint16_t CO2 = 0;
uint16_t tVOC = 0;

void CCS811_initialization()
{
	printf("Initializing CCS811...\n");
	_cmd = i2c_cmd_link_create();
	i2c_master_start(_cmd); //Request I2C start
	i2c_master_write_byte(_cmd, (CCS811_ADDR << 1) | I2C_MASTER_WRITE, 1); //@args cmd_handle - I2C cmd link, data - data to send, ack_en - enable ack check for master
	i2c_master_write_byte(_cmd, CCS811_SW_RESET, 1);
	i2c_master_write_byte(_cmd, 0x11, 1);
	i2c_master_write_byte(_cmd, 0xE5, 1);
	i2c_master_write_byte(_cmd, 0x72, 1);
	i2c_master_write_byte(_cmd, 0x8A, 1);
	i2c_master_stop(_cmd); //I2C command population is complete
	i2c_master_cmd_begin(I2C_NUM_0, _cmd, 1000 / portTICK_RATE_MS); //Ask ESP32 to perform I2C command
	i2c_cmd_link_delete(_cmd); //Release command handle

	vTaskDelay(1000/portTICK_PERIOD_MS);
}

/*
 * @Brief. Read one byte from register and put data at data pointer
 *
 * @param data pointer, register
 */
void readOneBye(uint8_t *data, uint8_t reg)
{
	_cmd = i2c_cmd_link_create();
	i2c_master_start(_cmd); //Request I2C start
	i2c_master_write_byte(_cmd, (CCS811_ADDR << 1) | I2C_MASTER_WRITE, 1);
	i2c_master_write_byte(_cmd, reg, 1);
	i2c_master_stop(_cmd);
	i2c_master_cmd_begin(I2C_NUM_0, _cmd, 500 / portTICK_RATE_MS); //Ask ESP32 to perform I2C command
	i2c_cmd_link_delete(_cmd); //Release command handle

	_cmd = i2c_cmd_link_create();
	i2c_master_start(_cmd); //Request I2C start
	i2c_master_write_byte(_cmd, (CCS811_ADDR << 1) | I2C_MASTER_READ, 1);
	i2c_master_read_byte(_cmd, data, 1);
	i2c_master_stop(_cmd);
	i2c_master_cmd_begin(I2C_NUM_0, _cmd, 500 / portTICK_RATE_MS); //Ask ESP32 to perform I2C command
	i2c_cmd_link_delete(_cmd); //Release command handle
}

/*
 * @Brief. Read ID register.
 * @param id pointer where to store the ID value.
 * @param reg ID register to read.
 */
void CCS811_getSensorID(uint8_t *id, uint8_t reg)
{
	readOneBye(id, reg);
}

/*
 * @Brief. Gets firmware information.
 * @param reg FW register to read.
 * return FW value.
 */
uint16_t CCS811_getFWinfo(uint8_t reg) //2 bytes read
{
	uint16_t data = 0;
	uint8_t high_data = 0;
	uint8_t low_data = 0;

	_cmd = i2c_cmd_link_create();
	i2c_master_start(_cmd); //Request I2C start
	i2c_master_write_byte(_cmd, (CCS811_ADDR << 1) | I2C_MASTER_WRITE, 1);
	i2c_master_write_byte(_cmd, reg, 1);
	i2c_master_stop(_cmd);
	i2c_master_cmd_begin(I2C_NUM_0, _cmd, 500 / portTICK_RATE_MS); //Ask ESP32 to perform I2C command
	i2c_cmd_link_delete(_cmd); //Release command handle

	_cmd = i2c_cmd_link_create();
	i2c_master_start(_cmd); //Request I2C start
	i2c_master_write_byte(_cmd, (CCS811_ADDR << 1) | I2C_MASTER_READ, 1);
	i2c_master_read_byte(_cmd, &high_data, 1);
	i2c_master_read_byte(_cmd, &low_data, 1);
	i2c_master_stop(_cmd);
	i2c_master_cmd_begin(I2C_NUM_0, _cmd, 500 / portTICK_RATE_MS); //Ask ESP32 to perform I2C command
	i2c_cmd_link_delete(_cmd); //Release command handle

	data = high_data << 8 | low_data;

	return data;
}

/*
 * @Brief. Function gets sensor id and prints it.
 */
void CCS811_printSensorId()
{
	uint8_t CCS811_ID = 0;
	uint8_t CCS811_Version = 0;
	uint16_t FWbootVer = 0;
	uint16_t FWapVer = 0;

	CCS811_getSensorID(&CCS811_ID, CCS811_HW_ID); //Read HW_ID register
	CCS811_getSensorID(&CCS811_Version, CCS811_HW_VERSION); //Read HW version register
	FWbootVer = CCS811_getFWinfo(CCS811_FW_BOOT_V); //Read boot version register
	FWapVer = CCS811_getFWinfo(CCS811_FW_APP_V); //Read FW APP version register

	if(CCS811_ID == 0x81) //Check if its CCS811
	{
		printf("CCS811 with id: %d.\n", CCS811_ID);
		printf("CCS811 version: %d.\n", CCS811_Version);
		printf("FW boot version. Major: %d. Minor: %d. Trivial: %d.\n", (FWbootVer&0xF000) >> 12, (FWbootVer&0x0F00) >> 8, FWbootVer&0x00FF);
		printf("FW app version. Major: %d. Minor: %d. Trivial: %d.\n", (FWapVer&0xF000) >> 12, (FWapVer&0x0F00) >> 8, FWapVer&0x00FF);
	}
}
/*
 * @Brief. Check status register for data ready. Return 1 if it is ready and 0 if data is not ready.
 * @return 1 if DATA_READY bit in STATUS register is set to 1.
 */
uint8_t dataReady()
{
	uint8_t status_reg = 0;
	readOneBye(&status_reg, 0x00);
#ifdef CCS811_DEBUGON
	printf("CCS811 Status reg: %d.\n", status_reg);
	printf("CCS811 Status reg moded: %d.\n", (status_reg & 1 << 3));
#endif
	if((status_reg & 0x0F) == 0x08)
	{
		return 1;
	}
	else
	{
		return 0;
	}

}

/*
 * @Brief. Checks if error bit is set in STATUS register.
 * return 1 if error bit is set in STATUS register.
 */
uint8_t readErrorIDRegister()
{
	uint8_t errorCode = 0;
	readOneBye(&errorCode, CCS811_ERROR_REG);
#ifdef CCS811_DEBUGON
	printf("ERROR: %d.\n", errorCode);
#endif
	return errorCode;
}

/*
 * @Brief. Reads ERROR register.
 * @Returns error id from ERROR_ID register.
 */
uint8_t checkError()
{
	uint8_t errorFlag = 0;
	readOneBye(&errorFlag, CCS811_ERROR_REG);
#ifdef CCS811_DEBUGON
	printf("ERROR: %d.\n", errorFlag);
#endif
	return (errorFlag & 1 << 0);
}

/*
 * @Returns current measurement mode from MEAS_MODE register
 */
uint8_t CCS811_getMEAS_MODE()
{
	uint8_t meas_modeData = 0;
	readOneBye(&meas_modeData, CCS811_MEAS_MODE);
#ifdef CCS811_DEBUGON
	printf("Meas_mode: %d.\n", meas_modeData);
#endif
	return meas_modeData;
}

/*
 * @Brief. Set up measurement mode.
 *
* Mode 0: 000 Idle (Measurements are disabled in this mode)
* Mode 1: 001 Constant power mode, IAQ measurement every second
* Mode 2: 010 Pulse heating mode IAQ measurement every 10 seconds
* Mode 3: 011 Low power pulse heating mode IAQ measurement every 60 seconds
* Mode 4: 100 Constant power mode, sensor measurement every 250ms (in mode 4 only RAW_DATA updates and not ALG_RESULT_DATA)
* 1xx: Reserved modes (For future use)
*/
void CCS811_setMEAS_MODE()
{
	uint8_t measMode = CCS811_getMEAS_MODE();
	measMode &= ~(0x07 << 4);
	measMode |= (CCS811_MODE << 4);

	_cmd = i2c_cmd_link_create();
	i2c_master_start(_cmd); //Request I2C start
	i2c_master_write_byte(_cmd, (CCS811_ADDR << 1) | I2C_MASTER_WRITE, 1); //@args cmd_handle - I2C cmd link, data - data to send, ack_en - enable ack check for master
	i2c_master_write_byte(_cmd, CCS811_MEAS_MODE, 1);
	i2c_master_write_byte(_cmd, measMode, 1);
	i2c_master_stop(_cmd); //I2C command population is complete
	i2c_master_cmd_begin(I2C_NUM_0, _cmd, 1000 / portTICK_RATE_MS); //Ask ESP32 to perform I2C command
	i2c_cmd_link_delete(_cmd); //Release command handle
}
/*
 * @Brief. Starts application inside the CCS811
 */
void CCS811_startApp()
{
	_cmd = i2c_cmd_link_create();
	i2c_master_start(_cmd); //Request I2C start
	i2c_master_write_byte(_cmd, (CCS811_ADDR << 1) | I2C_MASTER_WRITE, 1); //@args cmd_handle - I2C cmd link, data - data to send, ack_en - enable ack check for master
	i2c_master_write_byte(_cmd, CCS811_APP_START, 1);
	i2c_master_stop(_cmd); //I2C command population is complete
	i2c_master_cmd_begin(I2C_NUM_0, _cmd, 1000 / portTICK_RATE_MS); //Ask ESP32 to perform I2C command
	i2c_cmd_link_delete(_cmd); //Release command handle

	CCS811_setMEAS_MODE();
}

/*
 * @Brief. Read data from ALG_RESULT_DATA register.
 * Reads first 4 bytes of data (CO2 - high and low byte and tVOC - high and low byte)
 */
void CCS811_read_ALG_RESULT_DATA()
{
	uint8_t alg_data_1 = 0;
	uint8_t alg_data_2 = 0;
	uint8_t alg_data_3 = 0;
	uint8_t alg_data_4 = 0;

	_cmd = i2c_cmd_link_create();
	i2c_master_start(_cmd); //Request I2C start
	i2c_master_write_byte(_cmd, (CCS811_ADDR << 1) | I2C_MASTER_WRITE, 1);
	i2c_master_write_byte(_cmd, CCS811_ALG_RESULT_DATA, 1);
	//i2c_master_write_byte(_cmd, 0xC9, 1); <---
	i2c_master_stop(_cmd);
	i2c_master_cmd_begin(I2C_NUM_0, _cmd, 500 / portTICK_RATE_MS); //Ask ESP32 to perform I2C command
	i2c_cmd_link_delete(_cmd); //Release command handle

	_cmd = i2c_cmd_link_create();
	i2c_master_start(_cmd); //Request I2C start
	i2c_master_write_byte(_cmd, (CCS811_ADDR << 1) | I2C_MASTER_READ, 1);
	i2c_master_read_byte(_cmd, &alg_data_1, 0);
	i2c_master_read_byte(_cmd, &alg_data_2, 0);
	i2c_master_read_byte(_cmd, &alg_data_3, 0);
	i2c_master_read_byte(_cmd, &alg_data_4, 1);
	i2c_master_stop(_cmd);
	i2c_master_cmd_begin(I2C_NUM_0, _cmd, 500 / portTICK_RATE_MS); //Ask ESP32 to perform I2C command
	i2c_cmd_link_delete(_cmd); //Release command handle

#ifdef CCS811_DEBUGON
	printf("CCS811 ALG_RESULT_DATA1: %d.\n", alg_data_1);
	printf("CCS811 ALG_RESULT_DATA2: %d.\n", alg_data_2);
	printf("CCS811 ALG_RESULT_DATA3: %d.\n", alg_data_3);
	printf("CCS811 ALG_RESULT_DATA4: %d.\n", alg_data_4);
#endif

	CO2 = ((uint16_t)alg_data_1 << 8) | alg_data_2;
	tVOC = ((uint16_t)alg_data_3 << 8) | alg_data_4;

#ifdef CCS811_DEBUGON
	printf("CCS811 CO2: %d.\n", CO2);
	printf("CCS811 tVOC: %d.\n", tVOC);
#endif
}

/*
 * @Brief. Returns CO2 value.
 * @return CO2
 */
uint16_t CCS811_getCO2()
{
	return CO2;
}

/*
 * @Brief. Returns tVOC value
 * @return tVOC
 */
uint16_t CCS811_gettVOC()
{
	return tVOC;
}
